Programming Exercise: Address Book
---

## Setup
1. Install NodeJS
2. Install Dependencies
```bash
npm install
```

## Run Server
```bash
node server.js
```

## URL Endpoints
**Mockup:**
http://localhost:8080/mockup/

**People Data:**
http://localhost:8080/api/people

## Instructions for running angular application.
In addition to the instructions included above.

Perform the following commands to initiate the angular application from the root directory of the project.
1. cd address-book
2. npm install
3. npm start

This will start serving the application.  Typically in order to access the application
the url will be 'http://localhost:4200' although this can vary.
The specific url will be displayed after the application compiles in the terminal.

In order for the Angular application to function correctly the server hosting the express app
must also be running simultaneously in another terminal session.
Follow the instructions in the previous 'Run Server' section to run the express app.

To run the Angular unit tests execute:
npm test
from the address-book directory containing the angular application.

## Instructions
- Create a simple address book web application and use the given static
  HTML mockup (`mockup/index.html`) as a starting point or as inspiration.
- Your web application should fetch people data from http://localhost:8080/api/people
- Renders the names of all people from the people data in the left panel
  in alphabetical order.
- When a person's name is clicked in the left panel, render the full profile in the right panel.
- Update the `README.md` with any instructions for running the web application.
- Publish your solution to your Github or Bitbucket account or send us a zip file with your solution
  (you can use `zip.sh` to create an archive of this project).

### Bonus Points
- Add image URLs to the people data and render these photos in the profile
- Make it possible to change sort order of people shown in directory panel

### Additional Notes
- Feel free to use any framework or library (or use vanilla JavaScript)
- Feel free to modify `data/people.json` with any changes that you see fit.
- Feel free to add additional routes to the express app by modifying `server.js`
- Your address book does not need to use the exact same CSS or HTML as provided
  by the mockup.


