require('colors');
let path = require('path');
let express = require('express');
let people = require(path.join(__dirname, 'data/people.json'));
let utils = require('./utils');

let app = express();

app.use('/mockup/', express.static(path.join(__dirname, 'mockup')));
app.get('/api/people', function(req, res) {
    res.header('Access-Control-Allow-Origin', '*');
    res.status(200).send(JSON.stringify(people, null, '    '));
});

// Remove all whitespace from :name to use.
app.get('/api/people/:name', function(req, res) {
    res.header('Access-Control-Allow-Origin', '*');

    let matchingPeople = people.people.filter(person => utils.removeWhitespace(person.name)===req.params.name);
    if(matchingPeople){
        res.status(200).send(JSON.stringify(matchingPeople));
    }
    else{
        res.status(404).send('Person not found.');
    }
});

const pictureMap = new Map([
    ['XavierWright', 'Mauro-profile-picture.jpg'],
    ['AdamWright', 'businessman_1.jpg'],
    ['AbamWright', 'businessman_1.jpg'],
    ['JoeManfrey', 'businessman_2.jpg'],
    ['DouglasCho', 'pexels-photo-220453.jpg'],
    ['AllisonMurray', 'businesswoman_1.jpg']
]);

// Assumption: Assumes every name is unique.
// Remove all whitespace from :name to use.
app.get('/api/people/:name/profilePhoto', function(req, res) {
    res.header('Access-Control-Allow-Origin', '*');

    let options = {
        root: path.join(__dirname, 'data')
    };

    let pictureFile = pictureMap.get(req.params.name);
    if(pictureFile){
        res.status(200).sendFile(pictureFile, options);
    } else {
        res.status(404).send('Person not found.');
    }
});

let HTTP_PORT = 8080;

app.listen(HTTP_PORT, function(err) {
    if (err) {
        throw err;
    }

    console.log(('HTTP server listening on port ' + HTTP_PORT).green);

    console.log('Mockup:'.bold + ' http://localhost:' + HTTP_PORT + '/mockup/');
    console.log('People data:'.bold + ' http://localhost:' + HTTP_PORT + '/api/people');
    console.log('Specific person data:'.bold + ' http://localhost:' + HTTP_PORT + '/api/people/XavierWright');
    console.log('Specific person profile photo:'.bold + ' http://localhost:' + HTTP_PORT + '/api/people/XavierWright/profilePhoto');
});