export class Person {
  name: string;
  education: { institution: string,
               startYear: number,
               endYear?: number,
               degree: string }[];
  workExperience: { institution: string,
                    startYear: number,
                    endYear?: number,
                    title: string}[];
  department: string;
  phoneNumber: string;
  email: string;
}
