import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {DirectoryComponent, sortBy} from './directory.component';
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {Person} from "../../models/person.model";

// DOM testing and e2e testing would have been points I wanted to expand on with more time

let people: Person[] = [
  {
    "name": "Xavier Wright",
    "education": [
      {
        "institution": "NC State University",
        "startYear": 2001,
        "endYear": 2004,
        "degree": "Bachelor's, Computer Science"
      },
      {
        "institution": "Purdue",
        "startYear": 2004,
        "endYear": 2006,
        "degree": "Master's, Computer Science"
      }
    ],
    "workExperience": [
      {
        "institution": "Megacorp",
        "startYear": 2001,
        "endYear": 2005,
        "title": "Software Developer"
      },
      {
        "institution": "Ultra Megacorp",
        "startYear": 2005,
        "title": "Software Developer Lead"
      }

    ],
    "department": "Development",
    "phoneNumber": "111-111-1111",
    "email": "xavier.wright@mycompany.com"
  },
  {
    "name": "Adam Wrightc",
    "education": [
      {
        "institution": "NC State University",
        "startYear": 2001,
        "endYear": 2004,
        "degree": "Bachelor's, Computer Science"
      }
    ],
    "workExperience": [
      {
        "institution": "Megacorp",
        "startYear": 2001,
        "title": "Software Developer"
      }
    ],
    "department": "Development",
    "phoneNumber": "222-222-2222",
    "email": "adam.wright@mycompany.com"
  },
  {
    "name": "Abam Wrighta",
    "education": [
      {
        "institution": "NC State University",
        "startYear": 2001,
        "endYear": 2004,
        "degree": "Bachelor's, Computer Science"
      }
    ],
    "workExperience": [
      {
        "institution": "Megacorp",
        "startYear": 2001,
        "title": "Software Developer"
      }
    ],
    "department": "Development",
    "phoneNumber": "333-333-3333",
    "email": "abam.wright@mycompany.com"
  },
  {
    "name": "Joe Manfrey",
    "education": [
      {
        "institution": "Clemson University",
        "startYear": 1990,
        "endYear": 1995,
        "degree": "Bachelor's, Computer Science"
      }
    ],
    "workExperience": [
      {
        "institution": "Food Inc.",
        "startYear": 1998,
        "title": "Software Developer"
      }
    ],
    "department": "Development",
    "phoneNumber": "444-444-4444",
    "email": "joe.manfrey@mycompany.com"
  },
  {
    "name": "Douglas Cho",
    "education": [
      {
        "institution": "University of NC, Chapel Hill",
        "startYear": 1990,
        "endYear": 1995,
        "degree": "Marketing"
      }
    ],
    "workExperience": [
      {
        "institution": "Food Inc.",
        "startYear": 1998,
        "title": "Salesman"
      }
    ],
    "department": "Sales",
    "phoneNumber": "555-555-5555",
    "email": "douglas.cho@mycompany.com"
  },
  {
    "name": "Allison Murray",
    "education": [
      {
        "institution": "University of Southern California",
        "startYear": 2001,
        "endYear": 2005,
        "degree": "Sociology"
      }
    ],
    "workExperience": [
      {
        "institution": "United Products",
        "startYear": 1998,
        "title": "Directory of IT"
      }
    ],
    "department": "Development",
    "phoneNumber": "666-666-6666",
    "email": "allison.murray@mycompany.com"
  }
];

describe('DirectoryComponent', () => {
  let component: DirectoryComponent;
  let fixture: ComponentFixture<DirectoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectoryComponent ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('selecting person emits person from output', () => {
    let person: Person = null;

    component.selectedPerson.subscribe((next: Person) => {
      person = next;
    });

    component.selectPerson(people[0]);

    expect(person).toEqual(people[0]);
  });

  it('verify sorting first name a->z', () => {
    component.peopleData = JSON.parse(JSON.stringify(people));
    expect(component.separators).toEqual(['A', 'D', 'J', 'X']);
    expect(component.separatorsToPeople.get('A')).toEqual([people[2], people[1], people[5]]);
    expect(component.separatorsToPeople.get('D')).toEqual([people[4]]);
    expect(component.separatorsToPeople.get('J')).toEqual([people[3]]);
    expect(component.separatorsToPeople.get('X')).toEqual([people[0]]);
  });

  it('verify sorting first name z->a', () => {
    component.selectedSort = sortBy.FirstNameZA;
    component.peopleData = JSON.parse(JSON.stringify(people));
    expect(component.separators).toEqual(['X', 'J', 'D', 'A']);
    expect(component.separatorsToPeople.get('A')).toEqual([people[5], people[1], people[2]]);
    expect(component.separatorsToPeople.get('D')).toEqual([people[4]]);
    expect(component.separatorsToPeople.get('J')).toEqual([people[3]]);
    expect(component.separatorsToPeople.get('X')).toEqual([people[0]]);
  });

  it('verify sorting last name a->z', () => {
    component.selectedSort = sortBy.LastNameAZ;
    component.peopleData = JSON.parse(JSON.stringify(people));
    expect(component.separators).toEqual(['C', 'M', 'W']);
    expect(component.separatorsToPeople.get('C')).toEqual([people[4]]);
    expect(component.separatorsToPeople.get('M')).toEqual([people[3], people[5]]);
    expect(component.separatorsToPeople.get('W')).toEqual([people[0], people[2], people[1]]);
  });

  it('verify sorting last name z->a', () => {
    component.selectedSort = sortBy.LastNameZA;
    component.peopleData = JSON.parse(JSON.stringify(people));
    expect(component.separators).toEqual(['W', 'M', 'C']);
    expect(component.separatorsToPeople.get('C')).toEqual([people[4]]);
    expect(component.separatorsToPeople.get('M')).toEqual([people[5], people[3]]);
    expect(component.separatorsToPeople.get('W')).toEqual([people[1], people[2], people[0]]);
  });
});
