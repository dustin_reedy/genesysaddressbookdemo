import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/internal/Observable";
import { Person } from "../models/person.model";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  constructor(private httpClient: HttpClient) { }

  /**
   * Performs an http request to get people from an api.
   * Formats the response.
   * @returns {Observable<Person[]>}
   */
  getPeople(): Observable<Person[]> {
    return this.httpClient.get<any>('http://localhost:8080/api/people').pipe(map((response) => response.people));
  }
}
