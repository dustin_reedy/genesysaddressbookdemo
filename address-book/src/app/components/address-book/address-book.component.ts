import { Component, OnInit } from '@angular/core';
import { PeopleService } from "../../services/people.service";
import { Person } from "../../models/person.model";

@Component({
  selector: 'app-address-book',
  templateUrl: './address-book.component.html',
  styleUrls: ['./address-book.component.scss']
})
export class AddressBookComponent implements OnInit {

  constructor(private peopleService: PeopleService) { }

  people: Person[];
  selectedPerson: Person;

  ngOnInit(): void {
      // If the api respones wasn't near instanteous I would have included a loading spinner here.
      this.peopleService.getPeople().subscribe((people: Person[]) =>{
      this.people = people;
    });
  }

  /**
   * Sets the currently selected person in the address book.
   * @param person
   */
  onSelect(person: Person): void{
    this.selectedPerson = person;
  }

}
