import { TestBed } from '@angular/core/testing';

import { PeopleService } from './people.service';
import {HttpClientTestingModule, HttpTestingController, TestRequest} from "@angular/common/http/testing";
import {HttpClientModule} from "@angular/common/http";
import {Person} from "../models/person.model";

const peopleData = {
  "people": [
    {
      "name": "Xavier Wright",
      "education": [
        {
          "institution": "NC State University",
          "startYear": 2001,
          "endYear": 2004,
          "degree": "Bachelor's, Computer Science"
        },
        {
          "institution": "Purdue",
          "startYear": 2004,
          "endYear": 2006,
          "degree": "Master's, Computer Science"
        }
      ],
      "workExperience": [
        {
          "institution": "Megacorp",
          "startYear": 2001,
          "endYear": 2005,
          "title": "Software Developer"
        },
        {
          "institution": "Ultra Megacorp",
          "startYear": 2005,
          "title": "Software Developer Lead"
        }

      ],
      "department": "Development",
      "phoneNumber": "111-111-1111",
      "email": "xavier.wright@mycompany.com"
    },
    {
      "name": "Adam Wright",
      "education": [
        {
          "institution": "NC State University",
          "startYear": 2001,
          "endYear": 2004,
          "degree": "Bachelor's, Computer Science"
        }
      ],
      "workExperience": [
        {
          "institution": "Megacorp",
          "startYear": 2001,
          "title": "Software Developer"
        }
      ],
      "department": "Development",
      "phoneNumber": "222-222-2222",
      "email": "adam.wright@mycompany.com"
    },
    {
      "name": "Abam Wright",
      "education": [
        {
          "institution": "NC State University",
          "startYear": 2001,
          "endYear": 2004,
          "degree": "Bachelor's, Computer Science"
        }
      ],
      "workExperience": [
        {
          "institution": "Megacorp",
          "startYear": 2001,
          "title": "Software Developer"
        }
      ],
      "department": "Development",
      "phoneNumber": "333-333-3333",
      "email": "abam.wright@mycompany.com"
    },
    {
      "name": "Joe Manfrey",
      "education": [
        {
          "institution": "Clemson University",
          "startYear": 1990,
          "endYear": 1995,
          "degree": "Bachelor's, Computer Science"
        }
      ],
      "workExperience": [
        {
          "institution": "Food Inc.",
          "startYear": 1998,
          "title": "Software Developer"
        }
      ],
      "department": "Development",
      "phoneNumber": "444-444-4444",
      "email": "joe.manfrey@mycompany.com"
    },
    {
      "name": "Douglas Cho",
      "education": [
        {
          "institution": "University of NC, Chapel Hill",
          "startYear": 1990,
          "endYear": 1995,
          "degree": "Marketing"
        }
      ],
      "workExperience": [
        {
          "institution": "Food Inc.",
          "startYear": 1998,
          "title": "Salesman"
        }
      ],
      "department": "Sales",
      "phoneNumber": "555-555-5555",
      "email": "douglas.cho@mycompany.com"
    },
    {
      "name": "Allison Murray",
      "education": [
        {
          "institution": "University of Southern California",
          "startYear": 2001,
          "endYear": 2005,
          "degree": "Sociology"
        }
      ],
      "workExperience": [
        {
          "institution": "United Products",
          "startYear": 1998,
          "title": "Directory of IT"
        }
      ],
      "department": "Development",
      "phoneNumber": "666-666-6666",
      "email": "allison.murray@mycompany.com"
    }
  ]
};

// Note that this will require the server to be running to work.
// This is intentionally being skipped for now as a result.
xdescribe('PeopleService integration tests', () => {
  let service: PeopleService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule ]
    });
    service = TestBed.inject(PeopleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  // For reasons that are not entirely clear to me there seems to be some inconsistency with this test.
  // I thought it was a CORS issue, but even after setting
  // Access-Control-Allow-Origin to * on the server this is still occassionally failing
  // with a status code of 0, an unknown error.
  // It doesn't seem to have an effect on the actual application however.
  it('retrieve people from server', async() => {
    let peopleResponse: any = await service.getPeople().toPromise().catch(err => {
      console.log(err);
    });

    expect(peopleResponse).toBeTruthy();
    expect(peopleResponse).toEqual(peopleData);
  });

});

describe('PeopleService', () => {
  let service: PeopleService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    });
    service = TestBed.inject(PeopleService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should make api call when retrieving people', async()=> {
    let getPeoplePromise: Promise<Person[]> = service.getPeople().toPromise();

    let req: TestRequest =  httpMock.expectOne('http://localhost:8080/api/people');
    req.flush(peopleData);

    let people: Person[] = await getPeoplePromise;

    expect(people).toEqual(peopleData.people as Person[]);
  });
});
