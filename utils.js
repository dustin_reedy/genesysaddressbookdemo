function removeWhitespace(input){
    return input.replace(/\s+/g, '');
}

module.exports = {
    removeWhitespace
};