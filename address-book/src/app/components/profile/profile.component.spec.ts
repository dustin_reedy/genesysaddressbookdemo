import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileComponent } from './profile.component';
import {Person} from "../../models/person.model";

let person: Person = {
  "name": "Xavier Wright",
  "education": [
    {
      "institution": "NC State University",
      "startYear": 2001,
      "endYear": 2004,
      "degree": "Bachelor's, Computer Science"
    },
    {
      "institution": "Purdue",
      "startYear": 2004,
      "endYear": 2006,
      "degree": "Master's, Computer Science"
    }
  ],
  "workExperience": [
    {
      "institution": "Megacorp",
      "startYear": 2001,
      "endYear": 2005,
      "title": "Software Developer"
    },
    {
      "institution": "Ultra Megacorp",
      "startYear": 2005,
      "title": "Software Developer Lead"
    }

  ],
  "department": "Development",
  "phoneNumber": "111-111-1111",
  "email": "xavier.wright@mycompany.com"
};

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('getYearRangeString', () => {
    let startYear: number  = 2000;
    let endYear: number = 2005;

    it('formats year range with start and end included', () => {
      expect(component.getYearRangeString(startYear, endYear)).toEqual('2000-2005');
    });

    it('formats year range with only start included', () => {
      expect(component.getYearRangeString(startYear)).toEqual('2000-Present');
    });

    it('returns placeholder if start and end null', () => {
      expect(component.getYearRangeString(null, null)).toEqual('Missing Year Data');
    })
  });

  describe('getImageUrl', () => {
    it('returns url based on person\'s name', () => {
      component.person = person;
      expect(component.getImageUrl()).toEqual(`http://localhost:8080/api/people/XavierWright/profilePhoto`);
    });

    it('returns avatar image url if person not set', () => {
      expect(component.getImageUrl()).toEqual(`../../../assets/avatar.png`);
    });

    it('returns avatar image url if person has no name', () => {
      component.person = JSON.parse(JSON.stringify(person));
      component.person.name = null;
      expect(component.getImageUrl()).toEqual(`../../../assets/avatar.png`);
    });
  });
});
