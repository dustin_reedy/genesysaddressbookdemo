import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Person} from "../../models/person.model";
import * as utils from "../../../../../utils";

// Assumption: First name, last name sorting assumes always one space in person's name.
// This wouldn't be a safe assumption in the real world.

export enum sortBy {
  FirstNameAZ,
  FirstNameZA,
  LastNameAZ,
  LastNameZA
}

@Component({
  selector: 'app-directory',
  templateUrl: './directory.component.html',
  styleUrls: ['./directory.component.scss']
})
export class DirectoryComponent implements OnInit {
  sortByEnum = sortBy; // allows html to access enum values.

  private _peopleData: Person[];

  // custom setter automatically sorts when input changes.
  @Input() set peopleData(val: Person[]){
    if(val){
      let useFirstName: boolean = (this.selectedSort === sortBy.FirstNameAZ || this.selectedSort === sortBy.FirstNameZA);

      this._peopleData = val.sort((a: Person, b: Person) => {
        let isReverseSort: boolean = (this.selectedSort === sortBy.FirstNameZA || this.selectedSort === sortBy.LastNameZA);

        let nameToSort_A = a.name.split(' ')[(useFirstName)?0:1];
        let nameToSort_B = b.name.split(' ')[(useFirstName)?0:1];

        return this.compareAlphanumeric(nameToSort_A, nameToSort_B, isReverseSort);
      });

      this.separatorsToPeople = this.getPeopleGroups(this._peopleData, useFirstName);
      this.separators = [...this.separatorsToPeople.keys()];
    }
  }

  get peopleData(): Person[]{
    return this._peopleData;
  }

  @Output() selectedPerson: EventEmitter<Person> = new EventEmitter<Person>();

  separators: string[];
  /**
   * Map of seperator strings to person arrays.
   * @type {Map}
   */
  separatorsToPeople: Map<string, Person[]> = new Map<string, Person[]>();
  selectedSort: sortBy = sortBy.FirstNameAZ;

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * Emits the selected person from the directory.
   * @param person
   */
  selectPerson(person: Person): void {
    this.selectedPerson.next(person)
  }

  /**
   * Groups people into a map where the key is the string of the separator for that person.
   *
   * @param people
   * @param useFirstName - if this is true groups by first name.  If false groups by last name.
   * @returns {Map<string, Person[]>} - map where the key is the string of the separator for that person.
   */
  private getPeopleGroups(people: Person[], useFirstName?: boolean): Map<string, Person[]> {
    let groupMap: Map<string, Person[]> = new Map<string, Person[]>();

    if(people){
      people.forEach((person: Person) => {
        let firstChar = person.name.split(' ')[(useFirstName)?0:1].charAt(0).toUpperCase();

        if (firstChar) {
          let group: Person[] = groupMap.get(firstChar);
          if (group) {
            group.push(person);
          } else {
            groupMap.set(firstChar, [person]);
          }
        }
      });
    }

    return groupMap;
  }

  /**
   * Compares two string alphanumerically.  Does not compare whitespace characters.
   *
   * @param a
   * @param b
   * @param reverse - if true reverses the sort such that 'd' appears before 'c'.
   * @returns {number} - 1 if a > b. -1 if a < b. 0 if a and b equal.
   */
  private compareAlphanumeric(a: string, b: string, reverse?: boolean): number {
    let comparison = 0;

    let name_A: string = utils.removeWhitespace(a.toLowerCase());
    let name_B: string = utils.removeWhitespace(b.toLowerCase());

    if(name_A>name_B){
      comparison = 1;
    } else if (name_A<name_B){
      comparison = -1;
    }

    if(reverse){
      comparison *= -1;
    }

    return comparison;
  }
}
