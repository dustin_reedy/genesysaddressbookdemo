import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressBookComponent } from './address-book.component';
import {PeopleService} from "../../services/people.service";
import {of} from "rxjs/internal/observable/of";
import {Person} from "../../models/person.model";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";

const people: Person[] = [
  {
    "name": "Xavier Wright",
    "education": [
      {
        "institution": "NC State University",
        "startYear": 2001,
        "endYear": 2004,
        "degree": "Bachelor's, Computer Science"
      },
      {
        "institution": "Purdue",
        "startYear": 2004,
        "endYear": 2006,
        "degree": "Master's, Computer Science"
      }
    ],
    "workExperience": [
      {
        "institution": "Megacorp",
        "startYear": 2001,
        "endYear": 2005,
        "title": "Software Developer"
      },
      {
        "institution": "Ultra Megacorp",
        "startYear": 2005,
        "title": "Software Developer Lead"
      }

    ],
    "department": "Development",
    "phoneNumber": "111-111-1111",
    "email": "xavier.wright@mycompany.com"
  }
];

class PeopleServiceMock {
  getPeople() {
    return of(people);
  }
}

describe('AddressBookComponent', () => {
  let component: AddressBookComponent;
  let fixture: ComponentFixture<AddressBookComponent>;
  let peopleServiceMock: PeopleService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddressBookComponent ],
      providers: [
        { provide: PeopleService, useClass: PeopleServiceMock }
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();
    peopleServiceMock = TestBed.inject(PeopleService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressBookComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('retrieves people on initialization', async() => {
    let getPeopleSpy = spyOn(peopleServiceMock, "getPeople").and.callThrough();
    expect(getPeopleSpy).not.toHaveBeenCalled();
    fixture.detectChanges();
    expect(getPeopleSpy).toHaveBeenCalled();
    expect(component.people).toEqual(people);
  });

  it('selects person onSelect()', () => {
    component.onSelect(people[0]);
    expect(component.selectedPerson).toEqual(people[0]);
  });
});
