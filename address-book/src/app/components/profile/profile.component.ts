import {Component, Input, OnInit} from '@angular/core';
import {Person} from "../../models/person.model";
import * as utils from "../../../../../utils";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  @Input() person: Person;

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * Convert year range to string for display.  Ex: 2000-2005
   * If startYear does not exist returns 'Missing Year Data'
   * If startYear exists but endYear omitted show 'Present' for end year.
   *
   * @param startYear
   * @param endYear
   * @returns {string} - formatted year range string
   */
  getYearRangeString(startYear: number, endYear?: number): string {
    let yearString = 'Missing Year Data';

    if(startYear){
      yearString = `${startYear}-${endYear?endYear:'Present'}`;
    }

    return yearString;
  }

  /**
   * Returns image url for profile picture.
   * @returns {string} - image url for profile picture.
   */
  getImageUrl(): string {
    let url = '../../../assets/avatar.png';

    if(this.person && this.person.name){
      url = `http://localhost:8080/api/people/${utils.removeWhitespace(this.person.name)}/profilePhoto`;
    }

    return url;
  }
}
